#!/bin/bash
# install the ardour deps that had to be compiled
# two types:
# waf
# cmake/make
notneeded="
pkg-config-0.28
"
PYTHON_DIRS="
rdflib-4.1.2
"

MAKE_DIRS="
ExtUtils-MakeMaker-6.64
LRDF-0.5.1-rg
XML-Parser-2.41
XML-Simple-2.22
aubio-0.3.2
gtk-doc-1.21
gtk-engines-2.21.0
jpeg-9a
libart_lgpl-2.3.21
libiconv-1.14
liblo-0.28
nss-3.25/nspr
nss-3.25/nss
nss-pem-1.0.2
vamp-plugin-sdk-2.5
rubberband-1.8.1
"

WAF_DIRS="
lv2-1.14.0
serd-0.29.2
sord-0.16.1
sratom-0.6.0
suil-0.10.0
lilv-0.24.3
"

dir=$(pwd)
echo $PYTHON_DIR is done by hand

for i in $MAKE_DIRS
do
 cd $i
 echo
 echo =============== $i =============== 
 echo
 make install
 res=$?
 if [ $res != 0 ]; then exit; fi
 cd $dir
done

for i in $WAF_DIRS
do
 cd $i
 echo
 echo =============== $i =============== 
 echo
 ./waf install
 res=$?
 if [ $res != 0 ]; then exit; fi
 cd $dir
done
