# audiopi

Configuration scripts and tools for AudioPi, a raspberry pi 3 DAW running Alpine Linux, Ardour, Jack and other open source audio software

## Copyright (C) 2018, Samuel S Chessman
## Licensed under Gnu Public License 3.0 (GPLv3)
## Files

* README.md - source for README.html, coded in markdown
* LICENSE - License for distribution
* ardour_dep_urls.txt - urls of source distributions of dependencies not in repo
* cirrus.conf - modprobe dependency file for cirrus logic audio card
* devicetree - rc script to load any device tree overlay dtbo files at boot
* get\_alpine\_deps.sh  - Script to install dependencies from apk repository
* audiopi\_settings.sh
* pi\_temp.sh - Script to display RPi3 CPU and GPU temp in degrees C.
