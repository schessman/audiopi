#!/bin/bash

## GCC 9.3.0
#model name      : ARMv7 Processor rev 4 (v7l)
#BogoMIPS        : 38.40
# Features        : half thumb fastmult vfp edsp neon vfpv3 tls vfpv4 idiva idivt vfpd32 lpae evtstrm crc32 

OPT="-O2"
ARCH="-march=armv8-a+crc"
FPU="-ftree-vectorize"
TUNE="-mtune=cortex-a53"

export   CFLAGS="${OPT} ${ARCH} ${FPU} ${TUNE}"
export CXXFLAGS="${OPT} ${ARCH} ${FPU} ${TUNE}"
# build the ardour deps that need to be compiled with abuild
APORTS="/opt/alpine/aports/testing"
# order matters, have to install dependencies for some
ABUILDS="
aubio
liblo
lv2
serd
sord
sratom
lilv
rubberband
"

cd $APORTS
for dir in ${ABUILDS}
do
 cd $dir
 echo ${APORTS}/${dir}:
 sudo apk del $dir ${dir}-dev
 abuild clean
 abuild checksum
 time abuild -r >& abuild.out
 cat abuild.out
 cd ..
 sudo apk add $dir ${dir}-dev
done
