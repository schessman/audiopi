#!/bin/sh
# Setup headset out, line w/mic bias in on mixer 1
# This works with latest Cirrus Logic usecase scripts
# see http://www.horus.com/~hias/tmp/cirrus/cirrus-ng-scripts.tgz
# 
## Copyright (C) 2018, Samuel S Chessman
## Licensed under Gnu Public License 3.0 (GPLv3)
#
MYDIR=$(dirname "$0")
. "${MYDIR}/rpi-cirrus-functions.sh"

usage() {
	echo "usage: $0 [ output [ input [mixer-num] ] ]"
	echo "    output: line, headset, spdif, speaker; default is line"
	echo "    input: line, headset, spdif, dmic; default is line"
	echo "    mixer-num: 1-4, default is 1"
	exit 0
}

# check parameters
if [ $# -gt 3 ] ; then
	usage
fi

LISTEN_OUT="${1:-headset}"
LISTEN_IN="${2:-line}"
MIXER_NUM="${3:-1}"

# I2S interface RPi-WM5102 base name
rpi_if="AIF1"
rpi_in_if="${rpi_if}TX"
rpi_out_if="${rpi_if}RX"

# left+right mixer names
rpi_in_signals="${rpi_in_if}1 ${rpi_in_if}2"
rpi_out_signals="${rpi_out_if}1 ${rpi_out_if}2"

# I2S interface WM5102-WM8804 base name
spdif_if="AIF2"
spdif_in_if="${spdif_if}RX"
spdif_out_if="${spdif_if}TX"

case "$LISTEN_IN" in
    line|headset|spdif|dmic)
	;;
    *)
	usage
	;;
esac

case "$LISTEN_OUT" in
    line|headset|spdif|speaker)
	;;
    *)
	usage
	;;
esac

case "$MIXER_NUM" in
    1|2|3|4)
	;;
    *)
	usage
	;;
esac

# setup input
case "$LISTEN_IN" in
    line)
	record_from_linein_micbias
	#IN_SIGNALS=$line_in_signals
	#mixer "${line_in} High Performance Switch" on
	#mixer "Line Input Micbias" on
	# default input gain +64dB
	setup_line_in 64 128
	;;
    headset)
	IN_SIGNALS=$headset_in_signals
	# default input gain +20dB
	setup_headset_in 20 128
	;;
    spdif)
	IN_SIGNALS=$spdif_in_signals
	;;
    dmic)
	IN_SIGNALS=$dmic_in_signals
	# default input gain 0dB and digital gain -6dB
	setup_dmic_in 0 116
	;;
esac

# setup output
case "$LISTEN_OUT" in
    line)
	mixer "${line_out} Digital Volume" 128
	set_mixer $line_out_signals $IN_SIGNALS 32 $MIXER_NUM
	mixer "${line_out} Digital Switch" on
	;;
    headset)
	# use default path gain of -6dB for safety
	mixer "${headset_out} Digital Volume" 116
	setup_headset_out $rpi_out_signals
	#set_mixer $headset_out_signals $IN_SIGNALS 32 $MIXER_NUM
	#mixer "${headset_out} Digital Switch" on
	;;
    spdif)
	mixer "Tx Source" AIF
	set_mixer $spdif_out_signals $IN_SIGNALS 32 $MIXER_NUM
	;;
    speaker)
	# use default path gain of -6dB
	mixer "Speaker Digital Volume" "${1:-116}"
	set_mixer $speaker_out_signals $IN_SIGNALS 32 $MIXER_NUM
	mixer "Speaker Digital Switch" on
	;;
esac


